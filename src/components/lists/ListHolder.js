import React, { Component }         from 'react';
import ListItem                     from './ListItem';
import ReactTransitionGroup         from 'react-addons-transition-group';

export default class ListHolder extends Component {

  constructor(){
    super();
    this.renderList = this.renderList.bind(this);
  }


  renderList(item,index){

    return(
      <ListItem delay={index/10} mainDelay={this.props.mainDelay} key={index} item={item} speed={ {listSpeed:this.props.listSpeed, counterSpeed: this.props.counterSpeed} }/>
    );
  }


  render() {
    return (
        <ReactTransitionGroup component='ul' className='list-holder'>
          { this.props.items.map( this.renderList )}
        </ReactTransitionGroup>
    );
  }

}
