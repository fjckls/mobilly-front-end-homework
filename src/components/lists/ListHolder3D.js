import React, { Component }         from 'react';
import ListItem3d                   from './ListItem3D';
import ReactTransitionGroup         from 'react-addons-transition-group';

export default class ListHolder3D extends Component {

  constructor(){
    super();
    this.renderList = this.renderList.bind(this);
  }


  renderList(item,index){

    return(
      <ListItem3d delay={index/10} mainDelay={this.props.mainDelay} key={index} item={item} speed={ {listSpeed:this.props.listSpeed, counterSpeed: this.props.counterSpeed} }/>
    );
  }


  render() {
    return (
        <ReactTransitionGroup component='ul' className='list-holder list-holder-3d'>
          { this.props.items.map( this.renderList )}
        </ReactTransitionGroup>
    );
  }

}
