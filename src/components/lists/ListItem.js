import React, { Component }         from 'react';
import  TweenMax                  from 'gsap';

export default class ListItem extends Component {

  constructor(){
      super();
      this.handleMouseEnter = this.handleMouseEnter.bind(this);
      this.handleMouseOut = this.handleMouseOut.bind(this);
  }


  componentWillLeave(callback){
    TweenMax.to(this.refs.element, .3,
      { alpha:1, onComplete:callback}
    );
  }



  componentWillAppear(callback){

    let _this = this,
        delay = this.props.mainDelay ? this.props.mainDelay / 10 + this.props.delay : this.props.delay;

    TweenMax.fromTo(this.refs.element, this.props.speed.listSpeed,
      { y:130,  scale:.5,  alpha:0 },
      { y:0,    scale:1,   alpha:1, ease:Expo.easeOut, delay:delay, onComplete:callback}
    );


    // No meters set, don't tween meters - return
    if( !this.props.item.meters ){ return; }

    // Tween meters
    let counter = { var : 0 };
    TweenMax.to(counter, this.props.speed.counterSpeed, {
          var:    this.props.item.meters,
          delay:  delay,
          onUpdate: function () {

            // Display km instead of m if m > 1000
            let currentMeters = Math.floor(counter.var),
                isMeters = ( currentMeters / 1000 ) < 1;

            currentMeters = isMeters ? currentMeters : currentMeters / 1000;

            if(_this.refs.counter){
              _this.refs.counter.innerHTML = currentMeters + String( isMeters ? 'm' : 'km' );
            }


          },
          ease:Expo.easeIn
    });

  }

  handleMouseEnter(event){
    event.stopPropagation();
    TweenMax.to(this.refs.title, .4,{x:20, ease:Expo.easeOut});
  }

  handleMouseOut(event){
    event.stopPropagation();
    TweenMax.to(this.refs.title, .4,{x:0, ease:Expo.easeOut});
  }


  render() {
    const item = this.props.item;
    return (
        <li ref='element' className={ item.header ? 'list-header' : ''} onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseOut}>

          { item.badge ?
            <span className='round-badge-sm'>{item.badge}</span>
            : null }

          <span ref='title' className={ item.header ? 'list-header-title' : 'list-title'}>{item.title}</span>

          { item.meters ?
            <span className='list-badge-right'>~<span ref='counter'></span></span>
            : null }
        </li>
    );
  }
}
