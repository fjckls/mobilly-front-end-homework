import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';
import ReactTransitionGroup         from 'react-addons-transition-group';

/**
 * ACTION CREATORS
 */
 import { updateState }             from '../actions/main/ActionMain';

/**
 * COMPONENTS
 */
import Header                       from './common/Header';
import Parking                      from './pages/Parking';
import Trains                       from './pages/Trains';
import Bus                          from './pages/Bus';
import Taxi                         from './pages/Taxi';
import User                         from './pages/User';



class App extends Component {

  render() {
    let component = false;

    switch ( this.props.appState.pageId ) {

      case 'parking':
        component = <Parking/>;
        break;

      case 'trains':
        component = <Trains/>;
        break;

      case 'bus':
        component = <Bus/>;
        break;

      case 'taxi':
        component = <Taxi/>;
        break;

      case 'user':
        component = <User/>;
        break;

    }


    return (
        <div className='wrap'>

            <Header/>
            <ReactTransitionGroup>
                { component }
            </ReactTransitionGroup>

        </div>

    );
  }
}



function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( App );
