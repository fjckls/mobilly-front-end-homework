import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';


/**
 * ACTION CREATORS
 */
 import { updateState }             from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */


class Hamburger extends Component {

  constructor(){
      super();
      this.showMenu = this.showMenu.bind(this);
  }


  showMenu(event){
    event.preventDefault;
    this.props.updateState('menuVisible', !this.props.appState.menuVisible);
  }


  render() {
    return (
        <div id="hamburger" onClick={this.showMenu} className={this.props.appState.menuVisible ? 'open' : ''}>
          <span></span><span></span><span></span>
        </div>
    );
  }
}


function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Hamburger );
