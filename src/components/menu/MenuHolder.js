import React, { Component }         from 'react';
import MenuItem                     from './MenuItem';
import ReactTransitionGroup         from 'react-addons-transition-group';
import TweenMax                     from 'gsap';

const settings = [
  {icon:'car',    title:'Autostāvvietas',   id:'parking'},
  {icon:'train',  title:'Vilcieni',         id:'trains'},
  {icon:'bus',    title:'Autobusi',         id:'bus'},
  {icon:'taxi',   title:'Taksometri',       id:'taxi'},
  {icon:'male',   title:'Lietotājs',        id:'user'},
];

const speedIn   = .4;
const speedOut  = .3;

export default class MenuHolder extends Component {



  componentWillEnter (callback) {
      TweenMax.fromTo(this.refs.element, speedIn, {y: -100}, {y:0, onComplete: callback, ease: Expo.easeOut});
  }


  componentWillLeave (callback) {
    TweenMax.to(this.refs.element, speedOut, {y: -300, onComplete: callback, ease: Linear.easeOut});
  }



  renderItems(item, index){
    return(
      <MenuItem item={item} key={index}/>
    );
  }

  render() {
    return (
        <div className='menuHolder' ref='element'>

          <ul className='menu-list'>
            { settings.map(this.renderItems) }
          </ul>

        </div>

    );
  }
}
