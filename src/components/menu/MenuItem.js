import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';

/**
 * ACTION CREATORS
 */
import { updateState }              from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */



class MenuItem extends Component {

  constructor(){
      super();
      this.handleClickEvents = this.handleClickEvents.bind(this);
  }


  handleClickEvents(event){
    event.preventDefault();
    this.props.updateState('menuVisible', false);
    this.props.updateState('pageTitle',   this.props.item.title);
    this.props.updateState('pageId',      this.props.item.id);
  }


  render() {
    const item = this.props.item;

    return (
      <li className={ item.id == this.props.appState.pageId ? 'active-menu' : '' }>
        <a href='#' onClick={this.handleClickEvents}>
          <span className='menu-item-badge'>
            <i className={`fa fa-${item.icon}`} aria-hidden="true"></i>
          </span>
          <span className='menu-item-title'>{item.title}</span>
        </a>
      </li>
    );
  }
}

function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( MenuItem );
