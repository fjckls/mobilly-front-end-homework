import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';
import ReactTransitionGroup         from 'react-addons-transition-group';
import TweenMax from 'gsap';

/**
 * ACTION CREATORS
 */
import { updateState }              from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */
import Hamburger                    from '../menu/Hamburger';
import MenuHolder                   from '../menu/MenuHolder';

class Header extends Component {



  componentWillReceiveProps(newProps) {

    let delay=0;
    if( newProps.appState.menuVisible){
      for(let el in this.refs){
        delay++;
        TweenMax.fromTo(this.refs[el], .4, {y:-20}, {y:0, ease:Back.easeOut, delay:delay/40});
      }

    }else{
      for(let el in this.refs){
        delay++;
        TweenMax.fromTo(this.refs[el], .4, {y:0}, {y:-20, ease:Back.easeOut, delay:delay/40});
      }
    }
  }


  render() {


    return (
        <div id='header'>

          <div className='header-ui'>

            <div className='header-inner-wrap'>

              <Hamburger/>
              <div className='page-title-wrap'>
                  <div className={this.props.appState.menuVisible ? 'page-title-holder active-menu' : 'page-title-holder'}>
                    <h2 className='mobilly-logo'>
                      <span ref='m0'>M</span><span ref='m1'>O</span><span ref='m2'>B</span><span ref='m3'>I</span><span ref='m4'>L</span><span ref='m5'>L</span><span ref='m6'>Y</span>
                    </h2>
                    <h1 className='page-title'>{ this.props.appState.pageTitle }</h1>
                  </div>
              </div>

              <div className='top-badge'>14.38&euro;</div>

            </div>{/* .header-inner-wrap */ }



            <ReactTransitionGroup>
              { this.props.appState.menuVisible && <MenuHolder/>}
            </ReactTransitionGroup>
          </div>

        </div>

    );
  }
}

function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Header );
