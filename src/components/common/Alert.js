import React, { Component }         from 'react';


/**
 * ACTION CREATORS
 */


/**
 * COMPONENTS
 */


export default class Alert extends Component {

  constructor(){
    super();
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }


  showAlert(event){
    event.preventDefault();
    this.refs.btn.classList.add('is-open');
  }


  hideAlert(event){
    event.preventDefault();
    event.stopPropagation();
    this.refs.btn.classList.remove('is-open');
  }


  render() {

    return (

      <div className='alert-holder' >

        <div className="btn" data-direction="bottom" ref='btn' onClick={ this.showAlert }>
          <div className="btn-back">
            <p>{this.props.title}</p>
            <button className="no" onClick={ this.hideAlert }>{this.props.no}</button>
            <button className="yes" onClick={ this.hideAlert }>{this.props.yes}</button>
          </div>
          <div className="btn-front" onClick={ this.showAlert }>{this.props.button}</div>
        </div>

      </div>

    );
  }
}
