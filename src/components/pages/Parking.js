import React, { Component }         from 'react';
import ListHolder                   from '../lists/ListHolder3D';
import TweenMax                     from 'gsap';


const settings = {

  listSpeed     : .5,
  counterSpeed  : .5,

  zonesNear : [
    {title:'Tuvākās zonas(+-65M)', header:true },
    {title:'Europark biznesa ausgtskola Turība',    badge:'TU', meters:123},
    {title:'Citypark stāvvieta Bauskas ielā 88',    badge:'CI', meters:175},
    {title:'Rīgas satiksmes autostāvvieta',         badge:'A', meters:245},
    {title:'Rīgas satiksmes pazemes autostāvvieta', badge:'B', meters:368},
  ],

  zonesFar : [
    {title: 'Zonas', header:true },
    {title: 'Vecrīgas satiksmes autostāvvieta Vecrīgā',    badge: 'R',  meters: 1480},
    {title: 'EuroPark P.Brieža ielā 31',                   badge: 'AB', meters: 1713},
    {title: 'EuroPark rūpniecības ielā 32d',               badge: 'AF', meters: 1894},
    {title: 'Ciytparks Juglas ielā 20',                    badge: 'BG', meters: 2037},
  ]

}



export default class Parking extends Component {



  componentWillEnter (callback) {
      TweenMax.fromTo(this.refs.element, 0, {alpha:1}, {alpha:1, onComplete: callback});
  }


  componentWillLeave (callback) {
    TweenMax.to(this.refs.element, 0.2, {x: -600, alpha: 0, onComplete: callback, ease: Linear.easeOut});
  }



  render() {
    return (

      <span>



        <div ref='element'>
          <div className='zones-near'>
            <ListHolder is3d={true} items={settings.zonesNear} listSpeed={settings.listSpeed} counterSpeed={settings.counterSpeed}/>
          </div>{/* zonesNear */ }

          <div className='zones-far'>
            <ListHolder is3d={true} items={settings.zonesFar} mainDelay={settings.zonesNear.length} listSpeed={settings.listSpeed} counterSpeed={settings.counterSpeed}/>
          </div>{/* zonesFar */ }
        </div>


      </span>

    );
  }
}
