import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';


/**
 * ACTION CREATORS
 */
 import { updateState }             from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */
import Title from './common/Title';
import Input                        from './common/Input';
import ListHolder                   from '../lists/ListHolder';


const settings = {

  listSpeed     : .5,
  counterSpeed  : .5,

  nearestTaxi : [
    {title:'Tuvākie taksometri', header:true },
    {title:'Airbaltic',   badge:'AB',   meters:200},
    {title:'Taxi Panda',  badge:'P',    meters:348},
    {title:'Taxi Panda',  badge:'P',    meters:800},
    {title:'Red Cab',     badge:'RC',   meters:1259},
    {title:'Alviksa',     badge:'A',    meters:1879},
    {title:'Red Cab',     badge:'RC',   meters:2013},
  ],


}



class Taxi extends Component {

  render() {

    return (
      <span>
        <div className='page-holder'>
          <Title title='TAKSOMETRU APMAKSA'/>
        </div>

        <div className='input-wrap'>
          <Input label='Ievadiet no kurienes brauksiet'/>
          <Input label='Ievadiet uz kurieni brauksiet'/>
        </div>

        <div>
            <ListHolder items={settings.nearestTaxi} listSpeed={settings.listSpeed} counterSpeed={settings.counterSpeed}/>
        </div>

      </span>
    );
  }
}



function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Taxi );
