import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';


/**
 * ACTION CREATORS
 */
 import { updateState }             from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */
import Title                        from './common/Title';
import Input                        from './common/Input';
import Alert                        from '../common/Alert';


class User extends Component {

  render() {

    return (
        <span>
          <div className='page-holder'>
            <Title title='MANS KONTS'/>
          </div>

          <div className='input-wrap'>
            <Input label='Vārds'/>
            <Input label='Uzvārds'/>
            <Input label='Personas kods'/>
          </div>


          <Alert title='Vai mainīt savus datus?'  button='Saglabāt' yes='Jā' no='Nē'/>
        </span>

    );
  }
}



function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( User );
