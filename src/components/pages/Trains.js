import React, { Component }         from 'react';
import { connect }                  from 'react-redux';
import { bindActionCreators }       from 'redux';


/**
 * ACTION CREATORS
 */
 import { updateState }             from '../../actions/main/ActionMain';

/**
 * COMPONENTS
 */
import Title from './common/Title';
import Input from './common/Input';
import ListHolder                   from '../lists/ListHolder';


const settings = {

  listSpeed     : .5,
  counterSpeed  : .5,

  tickets : [
    {title:'Aktīvās biļetes', header:true },
    {title:'Tukums / Rīga',   badge:'TU'},
    {title:'Rīga / Tukums',   badge:'TU'},
    {title:'Smārde / Ogre',   badge:'TU'},
  ],


}





class Trains extends Component {



  render() {

    return (
      <span>

        <div className='page-holder' ref='element'>
          <Title title='VILCIENU BIĻETES'/>
        </div>

        <div className='input-wrap'>
          <Input label='Ievadiet sākuma staciju'/>
          <Input label='Ievadiet gala punktu'/>
        </div>

        <div>
            <ListHolder items={settings.tickets} listSpeed={settings.listSpeed} counterSpeed={settings.counterSpeed}/>
        </div>{/* zonesNear */ }


      </span>

    );
  }
}



function mapStateToProps( {  appState } ){
  return { appState }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ updateState }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Trains );
