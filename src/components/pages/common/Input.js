import React, { Component }         from 'react';

/**
 * ACTION CREATORS
 */


/**
 * COMPONENTS
 */





export default class Input extends Component {

  constructor(){
    super();
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onFocus( event ){
    if( event.target.value.replace(/ /g,'').length ) { return; }
    this.refs.parent.classList.add('focused-input');

  }

  onBlur(event){
    if( event.target.value.replace(/ /g,'').length ) { return; }
    this.refs.parent.classList.remove('focused-input');
  }

  render() {

    return (
        <span>

        <div className="input-holder floating-label" ref='parent'>
          <label>{ this.props.label }</label>
          <input type="text" onFocus={this.onFocus} onBlur={this.onBlur}/>
        </div>


        </span>

    );
  }
}
