import { combineReducers }  from 'redux';

import { AppState }  from './main/Main';


const rootReducer = combineReducers({
  appState : AppState
});

export default rootReducer;
