import _ from 'lodash';


function updateAppState(payload){
  return {
    type: 'UPDATE_APP_STATE',
    payload: payload,
  }
}


export function updateState( name, value ){


  return function(dispatch, getState){
    let state = getState();
    let appState = _.cloneDeep(state.appState);

    appState[name] = value;

    dispatch( updateAppState(appState) );

  }


}
