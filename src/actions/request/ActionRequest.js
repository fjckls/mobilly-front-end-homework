import axios from 'axios';

const ROOT_URL      = document.body.dataset.baseurl;
let   NAMESPACE     = 'default';

function responseOK(response){
  return {
    type: NAMESPACE + 'RESPONSE_OK',
    payload: response,
    error: false,
    isLoading:false,
  }
}


// Toggle loading status and preloader visibility
function loadingChangedStatus(isLoading) {

  return {
    type: NAMESPACE + 'IS_LOADING',
    isLoading:  isLoading,
    error: false,
  }
}

function toggleLoader(isLoading){
  if(isLoading){
    //$('.preloader').css('display', 'block');
  }else{
    //$('.preloader').css('display', 'none');
  }

}



function handleError(error){
  return {
    type: NAMESPACE + 'HANDLE_ERROR',
    error: error,
    isLoading: false,
  }
}












export function makeRequest( namespace, method, params ){

  NAMESPACE = namespace;


  // CALCULATE_PREMIUM_FINAL_
  return function(dispatch, getState){

      //const state = getState();

      dispatch( loadingChangedStatus(true) );

      toggleLoader(true);

      params.lang = document.body.dataset.lang;

      axios.post( ROOT_URL + method, {params:params} )
        .then(function(response){

          toggleLoader(false);


          if( response.data.error ){
            return dispatch(handleError(response.data.error));
          }

          dispatch(responseOK(response));

        })
        .catch(function (error) {
          if (error.response) {
            // The request was made, but the server responded with a status code
            // that falls out of the range of 2xx
            console.log('Error - 1 :', error.response.data);
            console.log('Error - 1 :', error.response.status);
            console.log('Error - 1 :', error.response.headers);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error - 2 :', error.message);
          }
          console.log('Error - 3 :',error.config);
          console.log('COUGHT ERRROR ', error)

          toggleLoader(false);
          dispatch(handleError('connection_error'));

        });
  }


}
